/*
    Crie uma função que dado uma string A e um array de strings,
    retorne um array novo com apenas as strings do array que são
    compostas exclusivamente por caracteres da string A

    Ex: ("ab", ["abc", "ba", "ab", "bb", "kb"]) => ["ba", "ab", "bb"]
        ("pkt", ["pkt", "pp", "pata", "po", "kkkkkkk"]) => ["pkt", pp, kkkkkkk]
*/


//Definição da função
function filtraarray (caracteres, vetor){
    let novoarray =  vetor.filter((e) => {
        let aux = true;
        for (i in e){
            if (! (caracteres.includes(e[i]))){
                aux = false;
            }
        }

        if (aux == true){
            return e;
        }
    })

    return novoarray;
}
   

//Exemplo para teste
let  a = "ab";
let array = ["abc", "ba", "ab", "bb", "kb"];
let result = filtraarray(a, array);

console.log(result);