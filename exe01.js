/*
    Criar uma função que, dado um número indefinido de arrays,
    faça a soma de seus valores
    Ex: [1,2,3] [1,2,2] [1,1] => 13
        [1,1] [2, 20] => 24
*/

//Definição da função
function soma(...vetores){
    let soma = 0;
    for (i in vetores){
        for (j in vetores[i]){
            soma += vetores[i][j];
        }
    }
    return soma;
}


//Exemplo para teste 
let array1 = [1,2,3];
let array2 = [1,2,2];
let array3 =  [1,1];
let result = soma(array1, array2, array3);

console.log(result);

