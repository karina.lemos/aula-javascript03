
/*
    Criar uma função que dado n arrays, retorne um novo array que possua
    apenas os valores que existem em todos os n arrays
    Ex: [1, 2, 3] [3, 3, 7] [9, 111, 3] => [3]
        [120, 120, 110, 2] [110, 2, 130] => [110, 2]
*/

//Definição da função
function valorcomum(...vetores) {
    let novoarray = [];
    for (i = 0; i < vetores[0].length; i++) {
        for (j = 1; j < vetores.length; j++) {
            if (!(vetores[j].includes(vetores[0][i]))) {
                break;
            } 
            if (!(novoarray.includes(vetores[0][i]))) {
                novoarray.push(vetores[0][i]);
            }
        }
    }
    return novoarray;
}


//Exemplo para teste
let vetor1 = [1, 2, 3];
let vetor2 = [3, 3, 7];
let vetor3 =[9, 11, 13];
let result = valorcomum(vetor1, vetor2, vetor3);

console.log(result);