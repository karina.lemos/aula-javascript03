
/*
    Criar uma função que dado um número n e um array, retorne
    um novo array com os valores do array anterior * n
    Ex: (2, [1,3,6,10]) => [2,6,12,20]
        (3, [7,9,11,-2]) => [21, 27, 33, -6]
*/


//Definição da função
function multiplicaarray (vetor, n){
    let novoarray = vetor.map((e) => {
        return e * n
    })
    return novoarray;
}


//Exemplo para teste: 
let array = [7,9,11,-2];
let n = 3;

console.log(multiplicaarray(array, n));