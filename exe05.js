
/*
    Crie uma função que dado n arrays, retorne apenas os que tenham a
    soma de seus elementos par
    Ex: [1, 1, 3] [1, 2, 2, 2, 3] [2] => [1, 2, 2, 2, 3] [2]
        [2,2,2,1] [3, 2, 1] => [3,2,1] 
*/


//Definição da função
function retornapar (...vetores){
    let novoarray =  vetores.filter((e) => {
        let somador = 0;
        e.forEach(element => {
            somador += element;
        })

        if (somador % 2 == 0){
            return e;
        }

    })
    return novoarray;
}


//Exemplo
let vetor1 = [1, 1, 3];
let vetor2 =  [1, 2, 2, 2, 3];
let vetor3 = [2];
let result = retornapar(vetor1, vetor2, vetor3);

console.log(result);